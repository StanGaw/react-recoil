const styles = {
  form: {
    display: "flex",
    flexDirection: "column",
  },
  textarea: {
    fontFamily: "Roboto",
    resize: "none",
    height: "150px",
    border: "1px solid #C4C4C4",
    borderRadius: 6,
    margin: "12px 0px 24px 0px",
    "&:hover": {
      borderColor: "black",
    },
  },
  input: {
    margin: "12px 0px 24px 0px",
  },
};
export default styles;
