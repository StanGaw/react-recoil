import React, { useState, useEffect } from "react";
import { useParams, useHistory } from "react-router-dom";
import { generate } from "shortid";
import { todayDate, convertDateObject } from "../../functions/functions";
import { useRecoilState } from "recoil";
import { taskState } from "../../App";
import { Box, TextField, Button, makeStyles } from "@material-ui/core";
import styles from "./styles";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";

const useStyles = makeStyles(styles);
const TaskForm = () => {
  const history = useHistory();
  const { id } = useParams();
  const [tasks, setTasks] = useRecoilState(taskState);

  const initialState = {
    title: "",
    expireDate: "",
    description: "",
    completed: false,
    active: true,
  };

  const [currentTask, setCurrentTask] = useState(initialState);
  const [selectedDate, setSelectedDate] = useState();
  const styles = useStyles();

  function editTask() {
    const editingTask = tasks.find((task) => task.id === id);
    let date;
    if (editingTask.expireDate !== undefined) {
      date = new Date(editingTask.expireDate);
    } else {
      date = todayDate();
    }
    setCurrentTask({
      ...editingTask,
    });
    setSelectedDate(date);
  }

  useEffect(() => {
    if (id && tasks.length > 0) {
      return editTask();
    } else {
      setCurrentTask({
        ...initialState,
        expireDate: todayDate(),
      });
    }
  }, [tasks, id]);

  const changeHandler = (e) => {
    setCurrentTask({
      ...currentTask,
      [e.target.name]: e.target.value,
    });
  };

  const dateHandler = (e) => {
    setSelectedDate(e);
    setCurrentTask({
      ...currentTask,
      expireDate: convertDateObject(e),
    });
  };

  const newTask = {
    ...currentTask,
    id: generate(),
  };

  const saveEditingTask = () => {
    const newState = [...tasks];
    newState.forEach((task, taskOrder) => {
      if (id === task.id) {
        newState.splice(taskOrder, 1, currentTask);
      }
      setTasks([...newState]);
    });
  };

  const saveTask = (e) => {
    e.preventDefault();
    if (id) {
      saveEditingTask();
      setCurrentTask(initialState);
    } else {
      setTasks([...tasks, newTask]);
    }
    setCurrentTask(initialState);
    history.push("/");
  };

  return (
    <Box>
      <form onSubmit={(e) => saveTask(e)} className={styles.form}>
        <label>Title</label>
        <TextField
          value={currentTask.title}
          onChange={(e) => changeHandler(e)}
          name="title"
          className={styles.input}
          size="small"
          variant="outlined"
        ></TextField>
        <label>Expiration Date</label>
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <KeyboardDatePicker
            disableToolbar
            variant="inline"
            format="MM/dd/yyyy"
            margin="normal"
            value={selectedDate}
            onChange={(e) => dateHandler(e)}
            KeyboardButtonProps={{
              "aria-label": "change date",
            }}
          />
        </MuiPickersUtilsProvider>
        <label>Description</label>
        <textarea
          value={currentTask.description}
          name="description"
          onChange={(e) => changeHandler(e)}
          className={styles.textarea}
          aria-label="empty textarea"
        />
        <Button type="submit" color="primary" variant="contained">
          Save
        </Button>
      </form>
    </Box>
  );
};

export default TaskForm;
