import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Task from "../../compontent/task/Task";
import Statistics from "../../compontent/task/Statistics";
import { useRecoilValue } from "recoil";
import { taskState } from "../../App";
import {
  makeStyles,
  TextField,
  Box,
  Typography,
  List,
  ListItem,
} from "@material-ui/core";
import styles from "./styles";

const useStyles = makeStyles(styles);
const Tasks = () => {
  const tasks = useRecoilValue(taskState);
  const [filter, setFilter] = useState({
    type: "active",
    value: true,
  });
  const [search, setSearch] = useState(false);

  const styles = useStyles();
  const searchFilter =
    tasks !== null &&
    tasks.filter((task) => task.title.toLowerCase().includes(filter.value));
  const optionFilter =
    tasks !== null &&
    tasks.filter((task) => task[filter.type] === filter.value);
  const filteredTaskList = search ? searchFilter : optionFilter;

  const searchHandler = (e) => {
    let newValue = e.target.value.toLowerCase();

    setSearch(true);
    setFilter({
      value: newValue,
    });
  };

  const noTasks = (
    <>
      <Box className={styles.placeHolderWrapper}>
        <Typography align="center" className={styles.placeholderText}>
          It seems You don't have any Tasks yet !
        </Typography>
        <Typography align="center" className={styles.placeholderText}>
          Please Add your first task
        </Typography>
      </Box>
      <Link className={styles.link} to="/create">
        Add
      </Link>
    </>
  );
  const noMatches = <p className={styles.nomatch}>No matches :(</p>;

  let listing =
    typeof filteredTaskList === "object" &&
    filteredTaskList.map((el) => {
      return (
        <ListItem disableGutters className={styles.item} key={el.id}>
          <Task item={el} />
        </ListItem>
      );
    });

  return (
    <Box className={styles.tasksView}>
      <Statistics filtering={setFilter} setSearch={setSearch} />
      <Box className={styles.main}>
        <Box className={styles.searchWrapper}>
          <TextField
            size="small"
            onChange={(e) => searchHandler(e)}
            className={styles.search}
            label="Search for Task..."
            variant="outlined"
          />
        </Box>
        <Box>
          <Typography variant="h5">
            <List className={styles.listWrapper}>
              {tasks !== null && tasks.length > 0
                ? filteredTaskList.length > 0
                  ? listing
                  : noMatches
                : noTasks}
            </List>
          </Typography>
        </Box>
      </Box>
    </Box>
  );
};

export default Tasks;
