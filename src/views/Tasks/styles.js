const styles = (theme) => ({
  searchWrapper: {
    display: "flex",
    width: "100%",
    maxHeight: 60,
    marginBottom: 24,
  },

  listWrapper: {
    maxHeight: 400,
    overflowY: "scroll",
    overflowX: "hidden",
    border: "1px solid #E8E8E8",
    borderRadius: 6,
    padding: 24,
    [theme.breakpoints.down("sm")]: {
      padding: 0,
      border: "none",
    },
  },

  search: {
    width: "100%",
    backgroundColor: "white",
  },

  button: {
    fontWeight: "bold",
    minWidth: 200,
    marginLeft: 24,
    maxHeight: 50,
    backgroundColor: "white",
    border: "2px solid #E8E8E8",
    "&:hover": {
      color: "#black",
      backgroundColor: "white",
      borderColor: "black",
    },
  },

  placeHolderWrapper: {
    marginBottom: 24,
  },

  placeholderText: {
    fontSize: 16,
    color: "#a2a2a2ee",
  },

  placeHolderButton: {
    backgroundColor: "#4ACA3F",
    color: "white",
    "&:hover": {
      backgroundColor: "#4aca3f",
    },
  },
  item: {
    display: "block",
  },
  link: {
    textDecoration: "none",
    color: "green",
    backgroundColor: "white",
    border: "2px solid green",
    padding: "8px 0",
    borderRadius: 6,
    display: "block",
    textAlign: "center",
    transition: "all .3s ",
    "&:hover": {
      backgroundColor: "green",
      color: "white",
      transition: "all .3s ",
    },
  },
  nomatch: {
    textAlign: "center",
    color: "gray",
  },
});

export default styles;
