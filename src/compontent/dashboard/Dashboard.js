import React from "react";
import { Link } from "react-router-dom";
import styles from "./styles";
import {
  Container,
  List,
  ListItem,
  Typography,
  Box,
  makeStyles,
} from "@material-ui/core";
const useStyles = makeStyles(styles);
const Dashboard = ({ children }) => {
  const styles = useStyles();
  return (
    <Container>
      <Typography className={styles.heading} variant="h4" component="h1">
        TODOS
      </Typography>
      <Box className={styles.wrapper}>
        <Box className={styles.sideBar}>
          <nav>
            <List className={styles.list}>
              <ListItem disableGutters>
                {" "}
                <Link className={styles.link} to="/">
                  List{" "}
                </Link>
              </ListItem>
              <ListItem disableGutters>
                <Link className={styles.link} to="/create">
                  Create New Task
                </Link>
              </ListItem>
            </List>
          </nav>
        </Box>
        <Box className={styles.childrenWrapper}>{children}</Box>
      </Box>
    </Container>
  );
};

export default Dashboard;
