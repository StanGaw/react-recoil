const styles = (theme) => ({
  heading: {
    padding: 35,
    borderBottom: "1px solid #E8E8E8",
    fontWeight: "bold",
  },
  sideBar: {
    maxWidth: '200px',
    width: "100%",
    height: "100%",
    [theme.breakpoints.down("sm")]:{
      maxWidth:'100%'
    }
  },
  wrapper: {
    display: "flex",
    marginTop: 40,
    [theme.breakpoints.down("sm")]: {
      flexDirection: "column",
      marginTop: 0,
    },
  },
  childrenWrapper: {
    width: "100%",
    maxWidth: "650px",
    marginLeft: 40,
    padding: "0px 40px 40px 60px",
    borderLeft: "1px solid #E8E8E8",
    [theme.breakpoints.down("sm")]: {
      padding: "0px",
      maxWidth: "100%",
      borderLeft: "none",
      marginLeft: "0",
    },
  },
  dashboardWrapper: {
    maxWidth: 1400,
    margin: "0 auto",
  },
  list: {
    borderBottom: "1px solid #E8E8E8",
    marginBottom: 24,
  },
  link: {
    textDecoration: "none",
    color: "black",
  },
});

export default styles;
