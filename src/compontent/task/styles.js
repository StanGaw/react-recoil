const styles = (theme) => ({
  task: {
    wordBreak: "break-word",
    display: "flex",
    border: "1px solid #E8E8E8",
    borderRadius: 6,
    transition: "all 0.3s ",
    cursor: "pointer",
    "&:hover": {
      borderColor: "black",
      transition: "all 0.3s ",
    },
  },
  accordionWrapper: {
    width: "100%",
  },
  description: {
    backgroundColor: "#f9f9f9",
    padding: 20,
  },
  heading: {
    marginRight: 24,
    maxWidth: 180,
    overflow: "ellipsis",
    [theme.breakpoints.down("sm")]: {
      marginBottom: 12,
      width: "100%",
    },
  },
  secondaryHeading: {
    color: "#4480ec",
  },
  customAccordionDetails: {
    flexDirection: "column",
  },
  descriptionHeading: {
    padding: 20,
    fontSize: 18,
    fontWeight: 500,
    backgroundColor: "#efefef",
  },
  iconsWrapper: {
    display: "flex",
    width: 100,
    justifyContent: "space-between",
    marginLeft: "auto",
    paddingRight: 16,
    [theme.breakpoints.down("sm")]: {
      padding: 0,
    },
  },
  delete: {
    transition: "all 0.3s ",
    color: "#E8E8E8",
    "&:hover": {
      color: "#FA7070",
      transition: "all 0.3s ",
    },
  },
  check: {
    transition: "all 0.3s ",
    color: "#E8E8E8",
    "&:hover": {
      color: "#4ACA3F",
      transition: "all 0.3s ",
    },
  },
  edit: {
    transition: "all 0.3s ",
    color: "#E8E8E8",
    "&:hover": {
      color: "black",
      transition: "all 0.3s ",
    },
  },

  stats: {
    justifyContent: "spaceBetween",
    padding: "20px 0px",
    width: "100%",
    overflowX: "scroll",
    [theme.breakpoints.down("sm")]: {
      borderBottom: "2px solid #E8E8E8",
      marginBottom: 24,
      padding: "0 0 20px 0",
      display: "flex",
    },
  },
  complete: {
    color: "green",
  },
  warning: {
    color: "red",
  },
  filter: {
    paddingRight: 25,
    whiteSpace: "nowrap",
    [theme.breakpoints.down("sm")]: {
      flexShrink: 0,
    },
  },
  summary: {
    flexWrap: "wrap",
  },
});

export default styles;
