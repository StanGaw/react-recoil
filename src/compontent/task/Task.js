import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { useRecoilState } from "recoil";
import { taskState } from "../../App";
import { saveLocaly } from "../../functions/functions";
import {
  Box,
  makeStyles,
  Typography,
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Tooltip,
} from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Delete from "@material-ui/icons/Delete";
import Edit from "@material-ui/icons/Edit";
import CheckCircle from "@material-ui/icons/CheckCircle";
import styles from "./styles";

const useStyles = makeStyles(styles);

const Task = ({ item }) => {
  const [tasks, setTasks] = useRecoilState(taskState);
  const [expanded, setExpanded] = useState(false);
  const styles = useStyles();
  const customClass = makeStyles(() => ({
    expanded: {},
    content: {
      flexWrap: "wrap",
    },
  }));
  const classes = customClass();
  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  useEffect(() => {
    saveLocaly(tasks);
  }, [tasks]);

  const complete = (e, index) => {
    e.stopPropagation(e);
    let newTaskState = {};
    const prevState = [...tasks];

    prevState.forEach((task, taskId) => {
      if (index === task.id) {
        newTaskState = {
          ...task,
          completed: true,
        };
        prevState[taskId] = newTaskState;
      }
    });

    setTasks([...prevState]);
  };

  const remove = (e, index) => {
    e.stopPropagation(e);
    const currentState = tasks.filter((task) => task.id !== index);
    setTasks([...currentState]);
    saveLocaly(currentState);
  };

  return (
    <Box className={styles.task}>
      <Accordion
        className={styles.accordionWrapper}
        expanded={expanded === "panel1"}
        onChange={handleChange("panel1")}
      >
        <AccordionSummary
          classes={{ content: classes.content }}
          expandIcon={<ExpandMoreIcon />}
        >
          <Typography noWrap className={styles.heading}>
            {item.title}
          </Typography>
          <Typography
            className={`${styles.secondaryHeading} ${
              item.completed ? styles.complete : styles.warning
            }`}
          >
            {item.expireDate}
          </Typography>
          <Box className={styles.iconsWrapper}>
            <Tooltip
              aria-label="complete"
              arrow
              placement="top"
              title="Completed"
            >
              <CheckCircle
                name="complete"
                onClick={(e) => complete(e, item.id)}
                className={styles.check}
              />
            </Tooltip>
            <Tooltip aria-label="edit" arrow placement="top" title="Edit">
              <Link to={`/edit/${item.id}`}>
                <Edit name="edit" className={styles.edit} />
              </Link>
            </Tooltip>
            <Tooltip
              aria-label="remove"
              arrow
              placement="top"
              position="top"
              title="Delete"
            >
              <Delete
                name="remove"
                onClick={(e) => remove(e, item.id)}
                className={styles.delete}
              />
            </Tooltip>
          </Box>
        </AccordionSummary>
        <AccordionDetails className={styles.customAccordionDetails}>
          <Typography className={styles.descriptionHeading}>
            Title : {item.title}
          </Typography>

          <Typography component="span" className={styles.description}>
            <Typography variant="h6" component="h2">
              Description :{" "}
            </Typography>
            {item.description}
          </Typography>
        </AccordionDetails>
      </Accordion>
    </Box>
  );
};

export default Task;
