import React from "react";
import { Box, Button, makeStyles } from "@material-ui/core";
import styles from "./styles";
import { useRecoilState } from "recoil";
import { taskState } from "../../App";
import {
  getTasksAmount,
  fetchTodos,
  saveLocaly,
} from "../../functions/functions";

const useStyles = makeStyles(styles);
const Statistics = ({ filtering, setSearch }) => {
  const styles = useStyles();
  const [tasks, setTasks] = useRecoilState(taskState);

  const clearData = () => {
    setTasks([]);
    saveLocaly([]);
  };
  async function getApiTodos() {
    const newState = await fetchTodos();
    if (tasks !== null) {
      setTasks([...tasks, ...newState]);
    } else {
      setTasks([...newState]);
    }
  }
  const active = () => {
    filtering({
      type: "active",
      value: true,
    });
    setSearch(false);
  };
  const completed = () => {
    filtering({
      type: "completed",
      value: true,
    });
    setSearch(false);
  };
  const uncompleted = () => {
    filtering({
      type: "completed",
      value: false,
    });
    setSearch(false);
  };
  return (
    <Box className={styles.stats}>
      <Button
        className={styles.filter}
        color="primary"
        onClick={() => active()}
      >
        All ({tasks !== null && tasks.length})
      </Button>
      <Button
        className={styles.filter}
        color="primary"
        onClick={() => completed()}
      >
        Completed ({getTasksAmount("completed", tasks !== null && tasks)})
      </Button>
      <Button
        className={styles.filter}
        color="secondary"
        onClick={() => uncompleted()}
      >
        Uncompleted (
        {getTasksAmount("completed", tasks !== null && tasks, false)})
      </Button>
      <Button className={styles.filter} onClick={() => getApiTodos()}>
        Get Some TODOS
      </Button>
      <Button className={styles.filter} onClick={() => clearData()}>
        Clear All
      </Button>
    </Box>
  );
};

export default Statistics;
