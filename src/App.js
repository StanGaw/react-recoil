import React, { useEffect } from "react";
import { atom, useRecoilState } from "recoil";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Dashboard from "./compontent/dashboard/Dashboard";
import Tasks from "./views/Tasks/Tasks";
import TaskForm from "./views/TaskForm/TaskForm";
import { localState } from "./functions/functions";
export const taskState = atom({
  key: "tasks",
  default: [],
});

const App = () => {
  const [tasks, setTasks] = useRecoilState(taskState);

  useEffect(() => {
    setTasks(localState());
  }, []);

  return (
    <Router>
      <Dashboard>
        <Switch>
          <Route exact path="/">
            <Tasks />
          </Route>
          <Route path="/create">
            <TaskForm />
          </Route>
          <Route path="/edit/:id">
            <TaskForm />
          </Route>
        </Switch>
      </Dashboard>
    </Router>
  );
};

export default App;
