import { generate } from "shortid";
export const saveLocaly = (tasks) => {
  let temp = [...tasks];
  temp = JSON.stringify(temp);
  localStorage.setItem("myTasks", temp);
};

export const localState = () => {
  let localState = localStorage.getItem("myTasks");
  if (
    localState !== null &&
    localState !== "" &&
    localState !== typeof Object
  ) {
    localState = JSON.parse(localState);
  }
  return localState;
};

export const checkActiveTasks = (tasks) => {
  let updatedTasks = [];
  tasks !== "null" &&
    tasks !== undefined &&
    tasks.forEach((task) => {
      if (task.expireDate < new Date()) {
        updatedTasks = [...updatedTasks, task];
      }
      return updatedTasks;
    });
};

export function pad(n, width, z) {
  z = z || "0";
  n = n + "";
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

export function convertDateObject(date) {
  let day = date.getDate();
  let month = date.getMonth() + 1;
  let year = date.getFullYear();
  let dateFormat = [];
  [month, day, year].forEach((item) => {
    item = pad(item, 2, "0");
    dateFormat = [...dateFormat, item];
  });
  return dateFormat.join("-");
}

export const todayDate = () => {
  let today = new Date();
  today = convertDateObject(today);
  return today;
};

export const getTasksAmount = (state, tasks, value = true) => {
  let array = [];
  tasks !== null &&
    tasks.length > 0 &&
    tasks.forEach((el) => {
      if (el[state] === value) {
        array = [...array, el];
      }
    });
  return array.length;
};

export async function fetchTodos() {
  const url = "https://gorest.co.in/public-api/todos";
  let response = await fetch(url);
  let { data } = await response.json();
  let dataContainer = [];
  data.forEach((item) => {
    const initialState = {
      title: item.title,
      expireDate: todayDate(),
      description: "",
      completed: item.completed,
      active: true,
      id: generate(),
    };
    dataContainer.push(initialState);
  });

  return dataContainer;
}
