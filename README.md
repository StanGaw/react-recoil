Todo-list application:
The user can add new tasks for a specific day, view completed and uncompleted tasks, edit and delete tasks.

Tech Stack:

React, 
React-hooks,
Recoil,
Material-UI,
API

To run the project localy instal node modules: 
   - npm install 
You can run project using : 
   - yarn start 
   - npm start